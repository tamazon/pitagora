﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cost : MonoBehaviour {

    public static int COST = 0;
    public Text CostT;

    private void Start()
    {
        CostT = GetComponent<UnityEngine.UI.Text>();
    }

    public static void ChangeCost(int n)
    {
        COST += n;
    }

    private void Update()
    {
        CostT.text = COST.ToString();
    }
}
