﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidPoints : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Sys.Down();
            Destroy(gameObject);
        }
    }

}
