﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class STageselect : MonoBehaviour {

    public void PushBackToSS()
    {
        JumpScenes("StageSelect");
    }

    public void PushStage1()
    {
        Sys.STAGESELECT(1);
        JumpScenes("Test");
    }

    public void PushStage2()
    {
        Sys.STAGESELECT(2);
        JumpScenes("Test");
    }

    public void JumpScenes(string n)
    {
        SceneManager.LoadScene(n);
    }
}
