﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageChange : MonoBehaviour {
    public float x, y, z;
    Vector3 P;

    private void Start()
    {
        P = new Vector3(x, y, z);
    }

    public void GoRight()
    {
        P = GetComponent<RectTransform>().localPosition;
        P.x += 1000;
        GetComponent<RectTransform>().localPosition = P;
    }

    public void GoLeft()
    {
        P = GetComponent<RectTransform>().localPosition;
        P.x -= 1000;
        GetComponent<RectTransform>().localPosition = P;
    }
}
