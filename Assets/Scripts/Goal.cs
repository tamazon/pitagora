﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            bool cg = Sys.CanGoal();

            if (cg == true)
            {
                SceneManager.LoadScene("Goal");
            }
        }
    }
}
