﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMoov : MonoBehaviour {

    public Slider slider;
    public static GameObject Ball;

    // Use this for initialization
    void Start () {
        slider.value = 0;     
	}

    public static void Setup()
    {
        Ball = Play.GetBall();
    } 
	
	// Update is called once per frame
	void Update () {
        int IsPlay = Play.GetIsPlay();
        if (IsPlay == 1)
        {

            transform.position = new Vector3(slider.value, 0, -10);

        }
        else
        {

            transform.position = new Vector3(Ball.transform.position.x, 0, -10);

        }
	}
}
