﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {

    public GameObject Ball,target,Sstart;

    private void Start()
    {
        Ball = Play.GetBall();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
           
            Vector3 SSPos = Sstart.transform.position;
            Ball.transform.position = SSPos;
            Vector3 Direction = target.transform.position - transform.position;

            Ball.GetComponent<Rigidbody>().AddForce(Direction, ForceMode.Impulse);
          

        }
    }
}
