﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Play : MonoBehaviour {

    public GameObject Stopper;
    public GameObject ball;
    public float FirstSpeed;
    private GameObject Ball;
    public static GameObject BALL;
    public Text T;
    public Vector3 Fpos;
    public static int ISplay = 1;

    private void Start()
    {
        Ball = Instantiate(ball, Fpos, Quaternion.identity);
        SetBALL(Ball);
        CameraMoov.Setup();
    }

    public void OnClick()
    {
        if(ISplay == 1)
        {

            Stopper.SetActive(false);
            T.text = "◇" + "\n" + "Reset";
            ISplay = 0;
            Ball.GetComponent<Rigidbody>().AddForce(new Vector3(FirstSpeed, 0, 0), ForceMode.Impulse);
           

        }else{

            Stopper.SetActive(true);
            Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            Ball.transform.position = Fpos;
            T.text = "▶" + "\n" + "Play";
            ISplay = 1;           
        }
    }

    public void SetBALL(GameObject n)
    {
        BALL = n;
    }

    public static GameObject GetBall()
    {
        return BALL;
    }

    public static int GetIsPlay()
    {
        return ISplay;
    }
}
