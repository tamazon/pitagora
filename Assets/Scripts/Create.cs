﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Create : MonoBehaviour {

    public GameObject Target;
    public Slider slider;
    public int ObjCost;
  
    public void OnClick()
    {
        float x = slider.value - 2.0f;
        Vector3 InstPoint = new Vector3(x,0,1);
        Instantiate(Target,InstPoint,Quaternion.identity);

        Cost.ChangeCost(ObjCost);
    }

}
