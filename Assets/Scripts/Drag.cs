﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Drag : MonoBehaviour {
    public float speed = 1;
    public GameObject D1,D2,d1,d2; //D1がDelZone
    public int ObjCost;
    GameObject[] MNs;
    Vector3 D2pos = new Vector3(0,0,5);
    public Text TestMSG;
    int preDegree;  

    private void Start()
    {
        MNs = GameObject.FindGameObjectsWithTag("Menu");
    }

    public void OnClick()
    {
        d1 =  Instantiate(D1);
        d2 = Instantiate(D2, D2pos, Quaternion.identity);
    }

    public void DrugNow()
    {
       

        foreach(GameObject mns in MNs)
        {
            mns.SetActive(false);
        }

        if (Input.touchCount >= 2)
        {

            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);
            Vector2 pos1 = touch1.position;
            Vector2 pos2 = touch2.position;

            RotateDirection rotateDirection = GetRotateDirection(pos1, pos2);

            if (rotateDirection == RotateDirection.LEFT)
            {
                Debug.Log("左回り");
                transform.Rotate(new Vector3(0, 0, 5));
            }
            else if (rotateDirection == RotateDirection.RIGHT)
            {
                Debug.Log("右回り");
                transform.Rotate(new Vector3(0, 0, -5));
            }
            else
            {
                // 回転なし
            }

        }
        else
        {

            Vector3 NowPos = transform.position;
            Vector3 TapScPos = Input.mousePosition;
            TapScPos.z = 11f;
            Vector3 TapWoPos = Camera.main.ScreenToWorldPoint(TapScPos);

            Vector3 Direction = TapWoPos - NowPos;

            transform.position += Direction * speed;

        }

    }

    public void DropNow()
    {
        Destroy(d1);
        Destroy(d2);

        foreach(GameObject mns in MNs)
        {
            mns.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "DeleteZone")
        {

            Destroy(d1);
            Destroy(d2);

            foreach (GameObject mns in MNs)
            {
                mns.SetActive(true);
            }

            Cost.ChangeCost(-ObjCost);
            Destroy(gameObject);
        }
    }

    enum RotateDirection
    {
        NONE,
        LEFT,
        RIGHT
    }


    public void Update()
    {

       
    }

    RotateDirection GetRotateDirection(Vector2 pos1, Vector2 pos2)
    {
        // x座標の位置でpos1とpos2を入れ替える
        if (pos1.x > pos2.x)
        {
            Vector2 tmp = pos1;
            pos1 = pos2;
            pos2 = tmp;
        }

        float dx = pos2.x - pos1.x;
        float dy = pos2.y - pos1.y;
        float rad = Mathf.Atan2(dy, dx);
        int degree = Mathf.RoundToInt(rad * Mathf.Rad2Deg);
        RotateDirection rotateDirection = RotateDirection.NONE;
        if (degree - preDegree > 0)
        {
            rotateDirection = RotateDirection.LEFT;
        }
        if (degree - preDegree < 0)
        {
            rotateDirection = RotateDirection.RIGHT;
        }
        preDegree = degree;
        return rotateDirection;
    }
}
