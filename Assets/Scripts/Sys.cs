﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sys : MonoBehaviour {

  
    GameObject[] MPoints;
    public static int MPn,STAGE;
    public static bool CG;
    public GameObject CreatedStage,SelectedStage,Stage1,Stage2;
  

    private void Start()
    {

        switch (STAGE)
        {
            case (0):
            case (1):
                SelectedStage = Stage1;
                break;
            case (2):
                SelectedStage = Stage2;
                break;
        }
        CreatedStage = Instantiate(SelectedStage);
       
    }

    public void PlayPush()
    {
        int IsPlay = Play.GetIsPlay();

        if (IsPlay == 1)
        { //以下でMPの通過状況などをリセット
          
            Destroy(CreatedStage);
          
            Instantiate(SelectedStage);

            MPoints = GameObject.FindGameObjectsWithTag("MidPoint");
            MPn = MPoints.Length;
           
        }
    }

    public static bool CanGoal()
    {
        if(MPn <= 0)
        {
            CG = true;
        }
        else
        {
            CG = false;
        }

        return CG;
    }

    public static void Down()
    {
        MPn -= 1;
    }

    public static void STAGESELECT(int n)
    {
        STAGE = n;
    }

}
